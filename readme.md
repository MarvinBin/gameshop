# Gameshop

## Scheduling

The Kanban scheduling system is used in this project. To see our Kanban board please visit [Trello](https://trello.com/b/IWzbVdc1/gameshop).

### Documentation
* [Laravel](http://laravel.com/docs)
* [AngularJS](https://docs.angularjs.org/guide)

#### License

Laravel and AngularJS are open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
