<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'platforms';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'slug',
		'description',
		'manufacturer_id'
	];

	/**
	 * Find a platform by its slug.
	 *
	 * @param  mixed  $slug
	 */
	public static function findBySlug($slug)
	{
		$platform = Platform::where('slug', $slug)->get()->first();
		return $platform;
	}

	public function games()
	{
		return $this->hasMany('App\Game');
	}

	public function manufacturer()
	{
		return $this->belongsTo('App\Manufacturer');
	}

}
