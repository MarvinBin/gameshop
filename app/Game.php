<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Game extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'games';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title_id',
		'slug',
		'description',
		'youtube_id_trailer',
		'price',
		'genre_id',
		'publisher_id',
		'platform_id',
		'box_art_path',
		'release_date'
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'release_date'
	];

	/**
	 * @param $slug
	 *
	 * @return mixed
	 */
	public static function getBySlug($slug)
	{
		$game = Game::where('slug', $slug)->get()->first();
		return $game;
	}

	/**
	 * @return bool
	 */
	public function getReleasedAttribute()
	{
		if ($this->release_date > Carbon::now()) {
			return false;
		}

		return true;
	}

	/**
	 * @return mixed
	 */
	public function getReleaseYearAttribute()
	{
		return $this->release_date->format('Y');
	}

	public function getDiscountPercentageAttribute()
	{
		if (! $this->getReleasedAttribute()) {
			$difference = $this->release_date->diffInDays();
			$week = Carbon::now()->addWeek()->diffInDays();
			$twoWeeks = Carbon::now()->addWeek(2)->diffInDays();
			$month = Carbon::now()->addMonth()->diffInDays();
			$twoMonths = Carbon::now()->addMonth(2)->diffInDays();

			if ($difference > $twoMonths) {
				return 40;
			} elseif ($difference > $month) {
				return 25;
			} elseif ($difference > $twoWeeks) {
				return 15;
			} elseif ($difference > $week) {
				return 5;
			}
		}
		return 0;
	}

	public function getDiscountedPriceAttribute()
	{
		return money_format('%i', $this->price - ($this->price * ($this->getDiscountPercentageAttribute() / 100)));
	}

	/**
	 * @return float
	 */
	public function getAverageStarsAttribute()
	{
		if (! $this->reviews->isEmpty()) {
			$total = 0;

			foreach($this->reviews as $review) {
				$total += $review->stars;
			}

			$average = $total / $this->reviews->count();

			return $average;
		}

		return 0;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function publisher()
	{
		return $this->belongsTo('App\Publisher');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function genre()
	{
		return $this->belongsTo('App\Genre');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function platform()
	{
		return $this->belongsTo('App\Platform');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function reviews()
	{
		return $this->hasMany('App\GameReview');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function screenshots()
	{
		return $this->hasMany('App\GameScreenshot');
	}

	public function storageLocations()
	{
		return $this->hasMany('\App\StorageLocation');
	}

}
