<?php

namespace App\Console\Commands;

use App\Game;
use App\OrderRule;
use App\StorageLocation;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Ixudra\Curl\Facades\Curl;
use Milon\Barcode\Facades\DNS1DFacade as DNS1D;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class Order extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'order';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Order new games to refill storage and check for new pre-orders';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->comment($this->handlePreOrders());
//		$this->comment($this->restock());
	}

	private function restock()
	{

		$locations = StorageLocation::where('physical_quantity', '<', 10)->get();

		$order = [
			'datum' => Carbon::now()->format('d-m-Y'),
			'klantnummer' => '103882',
			'adresgegevens' => [
				'bedrijf' => 'Gearbox Games',
				'straat' => 'Straat',
				'huisnummer' => 98,
				'postcode' => '1234 AA',
				'plaats' => 'Stad',
				'land' => 'Land'
			],
			'telefoonnummer' => '12-34567890',
		];

		foreach ($locations as $location) {
			$order['bestelling'][] = [
				'gameTitel' => $location->game->title,
				'aantal' => (10 - $location->physical_quantity),
			];
		};

		Curl::to('http://127.0.0.1:3000/store/order')
		    ->withData( ['q' => json_encode($order)] )
		    ->get();
	}

	/**
	 * @return mixed
	 */
	private function handlePreOrders()
	{
		$tomorrow = Carbon::now()->addDay()->format('Y-m-d%');
		$games = Game::where('release_date', 'like', $tomorrow)->get();

		foreach ($games as $game) {
			$orderRules = OrderRule::where('game_id', $game->id)->with('order')->get();

			foreach ($orderRules as $rule) {
				$barcode = DNS1D::getBarcodeHTML((string)$rule->order->id, 'C128', 6, 180);

				$order = $rule->order;

				$pdf = PDF::loadView('pdf.pre-order', compact([
					'barcode',
					'order',
					'rule',
				]));

				$user = $order->user;

				$randomPdfName = str_random(10) . '.pdf';

				Storage::disk('local')->put('/pdfs/' . $randomPdfName, $pdf->output());

				Mail::send('emails.pre-order',  ['user' => $user], function($m) use ($user, $order, $randomPdfName){
					$name = $user->first_name . ' ' . $user->last_name;
					$m->from('noreply@gameshop.dev', 'Gearbox Games');
					$m->attach('storage/app/pdfs/' . $randomPdfName);
					$m->to($user->email, $name)->subject('Uw pre-order is af te halen');
				});
			}
		}
	}


}
