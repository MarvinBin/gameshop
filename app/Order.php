<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orders';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id',
		'paid',
	];

	public function getTotalPriceAttribute()
	{
		$total = 0;

		foreach ($this->orderRules()->get() as $rule) {
			$total += ($rule->game->price * $rule->quantity);
		};

		return $total;
	}

	public function getTotalPriceRetrievableAttribute()
	{
		$total = 0;

		foreach ($this->orderRules()->get() as $rule) {
			if (! $rule->retrieved && $rule->game->released) {
				$total += ($rule->game->price * $rule->quantity);
			}
		};

		return $total;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function user()
	{
		return $this->belongsTo('\App\User');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function orderRules()
	{
		return $this->hasMany('\App\OrderRule');
	}
}
