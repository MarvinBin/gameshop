<?php

namespace App\Http\Controllers;

use App\FavoriteListItem;
use App\Game;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FavoriteListController extends Controller
{
    public function index()
    {
	    $items = FavoriteListItem::where('user_id', Auth::user()->id)->get();

        $games = $this->getGames($items);

	    return view('favorites.index', compact([
            'games',
        ]));
    }

    private function getGames($items)
    {
        $arr = [];

        foreach ($items as $item) {
            $arr[] = $item->game_id;
        }

        $games = Game::whereIn('id', $arr)->get();

        return $games;
    }
}
