<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
	/**
	 * Show the homepage.
	 *
	 * @return \Illuminate\View\View
	 */
	public function showHome()
    {
	    $games = Game::orderBy('release_date', 'desc')->get();

	    return view('pages.home', compact('games'));
    }
}
