<?php

namespace App\Http\Controllers;

use App\Platform;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PlatformController extends Controller
{
	/**
     * Show the platform belonging to the specified slug.
     *
     * @param $slug
     *
     * @return \Illuminate\View\View|void
     */
    public function showBySlug($slug)
    {
        $platform = Platform::findBySlug($slug);

        if (! $platform) {
            return abort(404);
        }

        return view('platforms.single', compact('platform'));
    }
}
