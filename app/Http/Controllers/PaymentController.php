<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function chooseMethod()
    {
	    $paymentMethods = [
		    'Fictieve betaalmethode',
	    ];

	    return view('payment.choose-method', compact([
		    'paymentMethods',
	    ]));
    }

	public function saveMethod(Request $request)
	{
		$request->session()->put('payment_method', $request->input('payment_method'));

		return redirect()->route('orders.confirm.get');
	}
}
