<?php

namespace App\Http\Controllers\Admin;

use App\Manufacturer;
use App\Platform;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PlatformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $platforms = Platform::all();

        return view('admin.platforms.index', compact('platforms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manufacturers = Manufacturer::all();

        return view('admin.platforms.create', compact('manufacturers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: ADD VALIDATION
        Platform::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'manufacturer_id' => $request->input('manufacturer')
        ]);

        // TODO: FLASH MESSAGE
        return redirect()->route('admin.platforms.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $platform = Platform::find($id);
        $manufacturers = Manufacturer::all();

        return view('admin.platforms.edit', compact([
            'platform',
            'manufacturers'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // TODO: ADD VALIDATION
        $platform = Platform::find($id);

        $platform->update([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'manufacturer_id' => $request->input('manufacturer')
        ]);

        // TODO: ADD FLASH MESSAGE
        return redirect()->route('admin.platforms.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Platform::destroy($id);

        // TODO: ADD FLASH MESSAGE
    }
}
