<?php

namespace App\Http\Controllers\Admin;

use App\Game;
use App\Genre;
use App\Platform;
use App\Publisher;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $games = Game::all();

        return view('admin.games.index', compact('games'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $genres = Genre::all();
        $publishers = Publisher::all();
        $platforms = Platform::all();

        return view('admin.games.create', compact([
            'genres',
            'publishers',
            'platforms'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // TODO: ADD VALIDATION
        $game = Game::create([
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'youtube_id_trailer' => $request->input('youtube-id-trailer'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'genre_id' => $request->input('genre'),
            'publisher_id' => $request->input('publisher'),
            'platform_id' => $request->input('platform'),
            'box_art_path' => $request->input('path-to-box-art'),
            'release_date' => $request->input('release-date')
        ]);

        // TODO: ADD FLASH MESSAGE
        return redirect()->route('admin.games.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $game = Game::find($id);
        $genres = Genre::all();
        $publishers = Publisher::all();
        $platforms = Platform::all();

        return view('admin.games.edit', compact([
            'game',
            'genres',
            'publishers',
            'platforms'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        // TODO: ADD VALIDATION
        $game = Game::find($id);

        $game->update([
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'youtube_id_trailer' => $request->input('youtube-id-trailer'),
            'price' => $request->input('price'),
            'genre_id' => $request->input('genre'),
            'publisher_id' => $request->input('publisher'),
            'platform_id' => $request->input('platform'),
            'box_art_path' => $request->input('path-to-box-art'),
            'release_date' => $request->input('release-date')
        ]);

        // TODO: ADD FLASH MESSAGE
        return redirect()->route('admin.games.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Game::destroy($id);

        // TODO: ADD FLASH MESSAGE
    }
}
