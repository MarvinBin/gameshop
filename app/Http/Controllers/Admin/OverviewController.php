<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OverviewController extends Controller
{
    protected $orders;

    public function dayOverview()
    {
        $this->orders = Order::where('created_at', 'like', Carbon::now()->format('Y-m-d%'))->get();

        $orders = $this->orders;

        $profit = $this->calculateProfit();

        return view('admin.overview.index', compact([
            'profit',
            'orders'
        ]));
    }

    private function calculateProfit()
    {
        $profit = 0;

        foreach ($this->orders as $order) {
            foreach ($order->orderRules as $orderRule) {
                $profit += $orderRule->game->price;
            }
        }

        return $profit;
    }

}
