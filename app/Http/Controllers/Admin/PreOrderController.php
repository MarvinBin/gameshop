<?php

namespace App\Http\Controllers\Admin;

use App\Discount;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PreOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discounts = $this->discountsPerPercentage();

        return view('admin.pre-orders.test', compact([
            'discounts'
        ]));
    }

    public function discountsPerPercentage()
    {
        $discounts = Discount::orderBy('percentage')->get();
        $arr = [];

        foreach($discounts as $discount) {
            if (array_key_exists((string)$discount->percentage, $arr)) {
                $arr[(string)$discount->percentage]['openstaand'] += $discount->orderRule->game->price;
                $arr[(string)$discount->percentage]['korting'] += money_format('%i', $discount->orderRule->game->price * $discount->percentage / 100);
            } else {
                $arr[(string)$discount->percentage] = [
                    'openstaand' => $discount->orderRule->game->price,
                    'korting' => money_format('%i', $discount->orderRule->game->price * $discount->percentage / 100)
                ];
            }
        }

        return $arr;
    }
}
