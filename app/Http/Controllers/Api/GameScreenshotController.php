<?php

namespace App\Http\Controllers\Api;

use App\GameScreenshot;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameScreenshotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $gameId
     *
     * @return \Illuminate\Http\Response
     */
    public function getByGameId($gameId)
    {
        $screenshots = GameScreenshot::where('game_id', $gameId)->get();

        return $screenshots;
    }
}
