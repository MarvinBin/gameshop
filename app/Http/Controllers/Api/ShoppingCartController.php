<?php

namespace App\Http\Controllers\Api;

use App\Game;

use Darryldecode\Cart\Facades\CartFacade as Cart;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShoppingCartController extends Controller
{
	/**
     * Add a game to the shopping cart.
     *
     * @param Request $request
     */
    public function addGame(Request $request)
    {
        $game = Game::find($request->input('id'));

        if (! $game) {
            return abort(404);
        }

        Cart::add($game->id, $game->title, $game->price, $request->input('quantity'), [
            'box_art' => $game->box_art_path,
            'platform_name' => $game->platform->name
        ]);
    }

    /**
     * Remove a game from the shopping cart.
     *
     * @param $id
     *
     * @internal param Request $request
     */
    public function removeGame($id)
    {
        Cart::remove($id);
    }

    public function updateQuantity(Request $request, $id)
    {
        Cart::update($id, [
            'quantity' => [
                'relative' => false,
                'value' => $request->input('quantity')
            ],
        ]);
    }

	/**
     * @return mixed
     */
    public function getContents()
    {
        $arr = [];

        foreach (Cart::getContent() as $item) {
            $arr[] = $item;
        }

        return $arr;
    }

}
