<?php

namespace App\Http\Controllers\Api;

use App\FavoriteListItem;
use App\Game;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FavoriteListController extends Controller
{
    public function store(Request $request)
    {
        $game = Game::find($request->input('id'));

        if (! $game) {
            return abort(404);
        }

        if (! Auth::check()) {
            return [
                'message' => 'Je moet ingelogd zijn om een game toe te voegen aan je lijst'
            ];
        }

        $item = FavoriteListItem::where('game_id', $game->id)->where('user_id', 1)->get();



        if ($item->isEmpty()) {
            $item = FavoriteListItem::create([
                'game_id' => $game->id,
                'user_id' => 1
            ]);

            return $item;
        }

        return [
            'message' => 'Deze game staat al op je lijst'
        ];
    }
}
