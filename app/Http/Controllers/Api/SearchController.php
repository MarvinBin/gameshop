<?php

namespace App\Http\Controllers\Api;

use App\Game;
use App\Platform;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
	/**
     * @param Request $request
     *
     * @return array|void
     */
    public function query(Request $request)
    {
        $query = $request->input('query');

        if (! $query) {
            return abort(404);
        }

        $searchResults = $this->getSearchResults($query);

        return $searchResults;
    }

    /**
     * @param $query
     *
     * @return array|void
     */
    private function getSearchResults($query)
    {
        $arr = [];

        $games = Game::where('title', 'like', '%'.$query.'%')->with('platform')->get();

        if (! $games->isEmpty()) {
            $arr['games'] = [
                'title' => 'Games',
                'data' => $games
            ];
        }

        $platforms = Platform::where('name', 'like', '%'.$query.'%')->with('manufacturer')->get();

        if (! $platforms->isEmpty()) {
            $arr['platforms'] = [
                'title' => 'Platforms',
                'data' => $platforms
            ];
        }

        if (! count($arr)) {
            return abort(404);
        }

        return $arr;
    }
}