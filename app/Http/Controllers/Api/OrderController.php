<?php

namespace App\Http\Controllers\Api;

use App\Order;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::where('id', $id)->with('user')->limit(1)->get()->first();

        if (! $order) {
            return abort(404);
        }

        return $order;
    }
}
