<?php

namespace App\Http\Controllers\Api;

use App\GameReview;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $gameId
     *
     * @return \Illuminate\Http\Response
     */
    public function show($gameId)
    {
        $reviews = GameReview::where('game_id', $gameId)->get();

        return $reviews;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gameReview = GameReview::create([
            'title' => $request->title,
            'body' => $request->body,
            'stars' => $request->rating,
            'game_id' => $request->gameId,
            'user_id' => 1
        ]);

        if (! $gameReview) {
            return abort(404);
        }

        return $gameReview;
    }
}
