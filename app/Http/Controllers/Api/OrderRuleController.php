<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\OrderRule;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OrderRuleController extends Controller
{
    /**
     * Get order rules for order.
     *
     * @param $orderId
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function showByOrderId($orderId)
    {
        $orderRules = OrderRule::where('order_id', $orderId)->with('game.storageLocations')->get();

        if (! $orderRules) {
            return abort(404);
        }

        return $orderRules;
    }

	/**
     * @param Request $request
     * @param $id
     */
    public function setRetrieved(Request $request, $id)
    {
        $orderRule = OrderRule::find($id);

        if (! $orderRule) {
            return abort(404);
        }

        DB::transaction(function() use ($request, $orderRule) {
            foreach($orderRule->game->storageLocations as $location) {
                if ($location->physical_quantity > 0) {
                    $location->physical_quantity -= $orderRule->quantity;
                    $location->save();

                    $orderRule->retrieved = true;
                    $orderRule->save();

                    continue;
                }
            }
        });

        return $orderRule;
    }
}
