<?php

namespace App\Http\Controllers\Api;

use App\Game;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $game = Game::find($id);

        if (! $game) {
            return abort(404);
        }

        return $game;
    }

    public function discountedPrice($id)
    {
        $game = Game::find($id);

        return money_format('%i', $game->price - ($game->price * ($game->discount_percentage / 100)));
    }

    public function checkIfReleased($id)
    {
        $game = Game::find($id);

        return (string)$game->released;
    }
}
