<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Game;

class GameController extends Controller
{
	/**
	 * Show the game that belongs to the specified platform and game slug.
	 *
	 * @param $platformSlug
	 * @param $gameSlug
	 *
	 * @return \Illuminate\View\View|void
	 */
	public function showBySlug($platformSlug, $gameSlug)
	{
		$game = Game::getBySlug($gameSlug);

		if (! $game || $game->platform->slug !== $platformSlug) {
			return abort(404);
		}

		return view('games.single', compact('game'));
	}
}
