<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Validator;

class AuthController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	protected $redirectPath = '/';

	protected $hashids;

	/**
	 * Create a new authentication controller instance.
	 */
	public function __construct() {
		$this->middleware( 'guest', [ 'except' => 'getLogout' ] );
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 *
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator( array $data ) {
		return Validator::make( $data, [
			'title_id' => 'required',
			'first_name' => 'required|max:255',
			'last_name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
			'date_of_birth' => 'required',
			'phone_number' => 'required',
			'street_name' => 'required',
			'house_number' => 'required',
			'zip_code' => 'required',
			'country' => 'required',
		] );
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 *
	 * @return User
	 */
	protected function create( array $data ) {
		return User::create( [
			'title_id' => $data['title_id'],
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'email' => $data['email'],
			'password' => bcrypt( $data['password'] ),
			'date_of_birth' => $data['date_of_birth'],
			'phone_number' => $data['phone_number'],
			'street_name' => $data['street_name'],
			'house_number' => $data['house_number'],
			'zip_code' => $data['zip_code'],
			'country' => $data['country'],
		] );
	}
}
