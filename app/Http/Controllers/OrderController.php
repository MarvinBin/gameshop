<?php

namespace App\Http\Controllers;

use App\Discount;
use App\Game;
use App\Order;
use App\OrderRule;
use Barryvdh\DomPDF\Facade as PDF;
use Darryldecode\Cart\Facades\CartFacade as Cart;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Milon\Barcode\Facades\DNS1DFacade as DNS1D;

class OrderController extends Controller
{
	// Variable to save the current order.
	protected $order;

	/**
	 * Show form to confirm order.
	 */
	public function getConfirm()
	{
		return view('orders.confirm');
	}

	/**
	 * Post to confirm order. Process the order in this method.
	 */
	public function postConfirm()
	{
		// Start DB transaction
		DB::transaction(
		function() {
			// Create Order
			$this->order = Order::create([
				'user_id' => Auth::user()->id,
				'paid' => true,
			]);

			// Make an OrderRule for every item in the Cart
			foreach (Cart::getContent() as $item) {
				$orderRule = OrderRule::create([
					'order_id' => $this->order->id,
					'game_id' => $item['id'],
					'quantity' => $item['quantity'],
					'retrieved' => false
				]);

				$game = Game::find($item['id']);

				if (! $game->released) {
					Discount::create([
						'order_rule_id' => $orderRule->id,
						'percentage' => $game->discount_percentage
					]);
				}

				foreach($orderRule->game->storageLocations as $location) {
					if ($location->sellable_quantity > 0) {
						$location->sellable_quantity -= $orderRule->quantity;
						$location->save();

						continue;
					}
				}
			}
		});

		// Remove all items in the cart.
		Cart::clear();

		// Generate PDF
		$this->generatePdf();

		// Send E-mail
		$this->sendMail();

		// Return page with a thank you message
		return redirect()->route('orders.thank-you');
	}

	private function sendMail()
	{
		$user = Auth::user();

		Mail::send('emails.order',  ['user' => $user], function($m) use ($user){
			$name = $user->first_name . ' ' . $user->last_name;
			$m->from('noreply@gameshop.dev', 'Gearbox Games');
			$m->attach('../storage/app/pdfs/' . $this->order->id . '.pdf');
			$m->to($user->email, $name)->subject('Bewijs van uw bestelling');
		});
	}

	private function generatePdf()
	{
		$barcode = DNS1D::getBarcodeHTML((string)$this->order->id, 'C128', 6, 180);

		$order = $this->order;

		$pdf = PDF::loadView('pdf.order', compact([
			'barcode',
			'order',
		]));

		Storage::disk('local')->put('/pdfs/' . $this->order->id . '.pdf', $pdf->output());
	}

	public function showThankYou()
	{
		return view('orders.thank-you');
	}
}
