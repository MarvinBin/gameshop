<?php

// Auth
Route::group([
	'prefix' => 'auth',
	'namespace' => 'Auth'
], function() {
	// Login
	Route::get('/login', [
		'as' => 'auth.login.get',
		'uses' => 'AuthController@getLogin'
	]);
	Route::post('/login', [
		'as' => 'auth.login.post',
		'uses' => 'AuthController@postLogin'
	]);

	// Logout
	Route::get('/logout', [
		'as' => 'auth.logout',
		'uses' => 'AuthController@getLogout'
	]);

	// Register
	Route::get('/register', [
		'as' => 'auth.register.get',
		'uses' => 'AuthController@getRegister'
	]);

	Route::post('/register', [
		'as' => 'auth.register.post',
		'uses' => 'AuthController@postRegister'
	]);
});

// Home
Route::get('/', [
	'as' => 'home',
	'uses' => 'PageController@showHome',
]);

// Favorites
Route::get('favorites', [
	'as' => 'favorites.index',
	'uses' => 'FavoriteListController@index',
	'middleware' => 'auth'
]);

// Shopping Cart
Route::get('/shopping-cart', [
	'as' => 'shopping-cart.index',
	'uses' => 'ShoppingCartController@index'
]);

// Payment
Route::get('payment/choose-method', [
	'as' => 'payment.choose-method',
	'uses' => 'PaymentController@chooseMethod',
]);

Route::post('payment/save-method', [
	'as' => 'payment.save-method',
	'uses' => 'PaymentController@saveMethod',
]);

// Order
Route::get('/orders/confirm', [
	'as' => 'orders.confirm.get',
	'uses' => 'OrderController@getConfirm',
]);

Route::post('/orders/confirm', [
	'as' => 'orders.confirm.post',
	'uses' => 'OrderController@postConfirm',
]);

Route::get('orders/thank-you', [
	'as' => 'orders.thank-you',
	'uses' => 'OrderController@showThankYou'
]);

// API
Route::group([
	'prefix' => 'api/v1',
	'namespace' => 'Api'
], function() {
	// Favorite list
	Route::post('favorite-list/store', [
		'as' => 'api.v1.favorite-list.store',
		'uses' => 'FavoriteListController@store'
	]);

	// Search
	Route::post('search', [
		'as' => 'api.v1.search.query',
		'uses' => 'SearchController@query'
	]);

	// Game Review Resource
	Route::resource('reviews', 'GameReviewController', [
		'only' => [
			'show',
			'store'
		]
	]);

	// Order
	Route::resource('orders', 'OrderController', [
		'only' => [
			'show',
		]
	]);

	// Order Rules
	Route::get('/order-rules/order/{orderId}', [
		'as' => 'order-rules.show-by-order-id',
		'uses' => 'OrderRuleController@showByOrderId'
	]);

	Route::post('/order-rules/{orderRuleId}/set-retrieved', [
		'as' => 'order-rules.set-retrieved',
		'uses' => 'OrderRuleController@setRetrieved'
	]);

	// Shopping Cart
	Route::post('/shopping-cart/add-game', [
		'as' => 'api.v1.shopping-cart.add-game',
		'uses' => 'ShoppingCartController@addGame'
	]);

	Route::post('/shopping-cart/update-quantity/{gameId}', [
		'as' => 'api.v1.shopping-cart.updateQuantity',
		'uses' => 'ShoppingCartController@updateQuantity'
	]);

	Route::delete('/shopping-cart/remove-game/{gameId}', [
		'as' => 'api.v1.shopping-cart.remove-game',
		'uses' => 'ShoppingCartController@removeGame'
	]);

	Route::get('/shopping-cart/get-contents', [
		'as' => 'api.v1.shopping-cart.get-contents',
		'uses' => 'ShoppingCartController@getContents'
	]);

	// Game Screenshot
	Route::get('/screenshots/{gameId}', [
		'as' => 'api.v1.screenshots.get-by-game-id',
		'uses' => 'GameScreenshotController@getByGameId'
	]);

	// Game Resource
	Route::resource('games', 'GameController', [
		'only' => [
			'show'
		]
	]);

	Route::get('games/discount/{id}', [
		'as' => 'api.v1.games.discount',
		'uses' => 'GameController@discountedPrice'
	]);

	Route::get('games/check-if-released/{id}', [
		'as' => 'api.v1.games.check-if-released',
		'uses' => 'GameController@checkIfReleased'
	]);
});

// Admin
Route::group([
	'prefix' => 'admin',
	'namespace' => 'Admin',
	'middleware' => 'auth'
], function() {
	// Root
	Route::get('/', function() {
		return redirect()->route('admin.dashboard.index');
	});

	// Dashboard
	Route::get('dashboard', [
		'as' => 'admin.dashboard.index',
		'uses' => 'DashboardController@index',
		'middleware' => 'shop.assistant'
	]);

	Route::get('pre-orders', [
		'as' => 'admin.pre-orders',
		'uses' => 'PreOrderController@index'
	]);

	// Games Resource
	Route::resource('games', 'GameController', [
		'except' => ['show'],
		'middleware' => 'admin'
	]);

	// Publishers Resource
	Route::resource('publishers', 'PublisherController', [
		'except' => ['show'],
		'middleware' => 'admin'
	]);

	// Platform Resource
	Route::resource('platforms', 'PlatformController', [
		'except' => ['show'],
		'middleware' => 'admin'
	]);

	// Manufacturer Resource
	Route::resource('manufacturers', 'ManufacturerController', [
		'except' => ['show'],
	]);

	// User Resource
	Route::resource('users', 'UserController', [
		'except' => ['show'],
		'middleware' => 'admin'
	]);

	// Order Resource
	Route::resource('orders', 'OrderController', [
		'except' => ['show'],
		'middleware' => 'admin'
	]);

	Route::get('day-overview', [
		'as' => 'admin.day-overview',
		'uses' => 'OverviewController@dayOverview'
	]);
});

// Single Platform
Route::get('/{platformSlug}', [
	'as' => 'platform.show-by-slug',
	'uses' => 'PlatformController@showBySlug',
]);

// Single Game
Route::get('/{platformSlug}/{gameSlug}', [
	'as' => 'game.show-by-slug',
	'uses' => 'GameController@showBySlug',
]);