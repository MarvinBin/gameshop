<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRule extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'order_rules';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'order_id',
		'game_id',
		'quantity',
		'paid',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function game()
	{
		return $this->belongsTo('\App\Game');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('\App\User');
	}

	public function order()
	{
		return $this->belongsTo('\App\Order');
	}
}
