<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'titles';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'text'
	];
}
