<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameReview extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'game_reviews';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
		'body',
		'stars',
		'game_id',
		'user_id',
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
