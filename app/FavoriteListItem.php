<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteListItem extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'favorite_list_items';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id',
		'game_id'
	];
}
