<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StorageLocation extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'storage_locations';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'game_id',
		'sellable_quantity',
		'physical_quantity',
	];

	public function game()
	{
		return $this->belongsTo('\App\Game');
	}
}
