<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameScreenshot extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'game_screenshots';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id',
		'image_path'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function game()
	{
		return $this->belongsTo('App\Game');
	}
}
