<?php

$factory->define(App\GameReview::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'body' => $faker->text(),
        'stars' => rand(1, 5),
        'game_id' => rand(1, \App\Game::all()->count()),
        'user_id' => rand(1, \App\User::all()->count())
    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'name' => $faker->lastName,
        'email' => $faker->email,
        'password' => Hash::make('secret'),
        'remember_token' => str_random(10),
    ];
});
