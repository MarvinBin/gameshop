<?php

use Illuminate\Database\Seeder;

class GameReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\GameReview::class, 100)->create();
    }
}
