<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use App\Game;

class GameTableSeeder extends Seeder {
	/**
	 * Array with default games.
	 *
	 * @var array $games
	 */
	protected $games;

	/**
	 * Constructor
	 *
	 * @param Faker $faker
	 */
	public function __construct( Faker $faker ) {
		$this->games = [
			[
				'title'        => 'Project Cars',
				'slug'         => 'project-cars',
				'description'  => 'Project CARS is the most authentic, beautiful, intense, and technically-advanced racing game on the planet. Create a driver, pick from a variety of motorsports, and shift into high gear to chase a number of Historic Goals and enter the Hall Of Fame. Then test your skills online either in competitive fully-loaded race weekends, leaderboard-based time challenges, or continually-updated community events. Featuring world-class graphics and handling, a ground-breaking dynamic time of day & weather system, and deep tuning & pit stop functionality, Project CARS leaves the competition behind in the dust.',
				'price'        => 39.99,
				'genre_id'     => 1,
				'publisher_id' => 1,
				'platform_id'  => 4,
				'box_art_path' => 'http://s.s-bol.com/imgbase0/imagebase3/large/FC/2/5/6/2/9200000032162652.jpg',
				'release_date' => '2014-11-15',
				'youtube_id_trailer' => 'BDIXSouz5LA',
			],
			[
				'title'        => 'Need for Speed 2015',
				'slug'         => 'nfs-2015',
				'description'  => 'Need for Speed delivers on what fans have been requesting and what the franchise stands for - deep customization, an authentic open world filled with real world car culture, and a narrative that drives your game.',
				'price'        => 59.99,
				'genre_id'     => 1,
				'publisher_id' => 2,
				'platform_id'  => 4,
				'box_art_path' => 'http://s.s-bol.com/imgbase0/imagebase3/large/FC/1/9/6/7/9200000045947691.jpg',
				'release_date' => '2015-11-03',
				'youtube_id_trailer' => 'F3gy5_fzW6g',
			],
			[
				'title'        => 'Gran Turismo',
				'slug'         => 'gt-1',
				'description'  => 'Gran Turismo (GT1) is a racing game designed by Kazunori Yamauchi. Gran Turismo was developed and published by Sony Computer Entertainment in 1997 for the PlayStation video game console. The game\'s development group was established as Polyphony Digital in 1998.',
				'price'        => 29.99,
				'genre_id'     => 1,
				'publisher_id' => 4,
				'platform_id'  => 1,
				'box_art_path' => 'https://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Gran_Turismo_-_Cover_-_North_America.jpg/256px-Gran_Turismo_-_Cover_-_North_America.jpg',
				'release_date' => '1997-12-23',
				'youtube_id_trailer' => 'KhGjqtkgpt4',
			],
			[
				'title'        => 'FlatOut',
				'slug'         => 'flatout',
				'description'  => 'FlatOut is a racing video game developed by the Finnish developer Bugbear Entertainment and published by Empire Interactive and Vivendi Universal Games in 2004 and 2005. Gameplay in FlatOut places emphasis on demolition derby-style races, and features a sophisticated physics engine. 16 different cars are included, each with 5 different skins for them. The game is most known for car drivers flying through the windshield.',
				'price'        => 29.99,
				'genre_id'     => 1,
				'publisher_id' => 5,
				'platform_id'  => 2,
				'box_art_path' => 'http://i111.twenga.com/video-games/jogos-de-ps2/-flatout-ps2-d0671-tp_6198775379237970130f.jpg',
				'release_date' => '2005-07-12',
				'youtube_id_trailer' => 'tOTiVOGRdcI',
			],
			[
				'title'        => 'Need for Speed: Rivals',
				'slug'         => 'nfs-rivals-ps3',
				'description'  => 'Need for Speed Rivals is an open world racing video game. Developed by Ghost Games and Criterion Games, it is the twentieth installment in the Need for Speed series. The game was released for Microsoft Windows, PlayStation 3 and Xbox 360 on 19 November 2013, and for PlayStation 4 and Xbox One as launch titles in the same month.Need for Speed Rivals was met with a positive reception upon release, with critics mostly praising the visuals, controls, and online systems.',
				'price'        => 24.99,
				'genre_id'     => 1,
				'publisher_id' => 2,
				'platform_id'  => 3,
				'box_art_path' => 'http://s.s-bol.com/imgbase0/imagebase3/large/FC/9/9/3/8/9200000014978399.jpg',
				'release_date' => '2013-11-12',
				'youtube_id_trailer' => 'xsKHweFi7AU',
			],
			[
				'title'        => 'The Crew, Wild Run',
				'slug'         => 'the-crew-wild-run',
				'description'  => 'The Crew Wild Run Edition is je go-to editie voor de iconische actie-race game die uitgekomen is in 2014. Deze volledig uitgebreide editie van The Crew bevat de originele game met meer dan 50 missies en modi die vanaf launch beschikbaar zijn, plus de complete nieuwe The Crew Wild Run-uitbreiding. Krijg meer dan 70 verschillende voertuigen met meer dan 200 tuning specs, race in een 30 uur durende campagne met meer dan 210 missies en neem deel aan terugkomend kampioenschat ‘The Summit’. Elke maand zijn er nieuwe events en ongelimiteerde ‘Freedrive’ activiteiten in het beste race-speelterrein ooit gemaakt met 5.000 km2 aan open wereld.',
				'price'        => 45.99,
				'genre_id'     => 1,
				'publisher_id' => 3,
				'platform_id'  => 4,
				'box_art_path' => 'http://s.s-bol.com/imgbase0/imagebase3/large/FC/4/2/5/8/9200000047948524.jpg',
				'release_date' => '2015-11-17',
				'youtube_id_trailer' => 'DuU9NUER4PI',
			],
			[
				'title'        => 'Forza Motorsport 6',
				'slug'         => 'forza-6',
				'description'  => 'Van de makers van de bestverkopende en prijzen winnende Xbox raceserie, komt nu het vervolg uit: Forza Motorsport 6. Verzamel, customize en race de wagens van je dromen en ervaar het videogame debuut van de Ford GT.',
				'price'        => 59.99,
				'genre_id'     => 1,
				'publisher_id' => 6,
				'platform_id'  => 5,
				'box_art_path' => 'http://s.s-bol.com/imgbase0/imagebase3/large/FC/8/8/7/2/9200000038822788.jpg',
				'release_date' => '2015-09-11',
				'youtube_id_trailer' => '0CXqEHMgiFg',
			],
			[
				'title'        => 'Formula 1 - F1 2015',
				'slug'         => 'formula-1-2015',
				'description'  => 'F1 2015 is gebaseerd op het Formule 1-seizoen van 2015 en bevat alle coureurs en racebanen die hierbij centraal staan. Je zult dus beroemdheden als Felipe Massa, Lewis Hamilton en Max Verstappen tegenkomen en kunt tegen hen racen op onder andere het Autódromo Hermanos Rodríguez. Bovendien bevat de game als bonus alle coureurs, auto\'s en circuits uit het 2014-seizoen.',
				'price'        => 59.99,
				'genre_id'     => 1,
				'publisher_id' => 7,
				'platform_id'  => 4,
				'box_art_path' => 'http://s.s-bol.com/imgbase0/imagebase3/large/FC/2/6/1/8/9200000035818162.jpg',
				'release_date' => '2015-07-10',
				'youtube_id_trailer' => 'UjVmodaEOO8',
			],
			[
				'title'        => 'Crash Team Racing',
				'slug'         => 'crash-team-racing',
				'description'  => 'Crash Team Racing is the first racing game in the Crash Bandicoot franchise, released on the PlayStation in 1999 and the 4th installment to the series, as well as the last to be developed by Naughty Dog. Crash Team Racing became a \'Greatest Hits\' title in 2000 in the US, a \'PlayStation, The Best\' title in 2000 in Japan and a \'Platinum\' title in Europe and the overall PAL region late 2000. For the Japanese localization, the game was renamed Crash Bandicoot Racing.',
				'price'        => 19.96,
				'genre_id'     => 1,
				'publisher_id' => 4,
				'platform_id'  => 1,
				'box_art_path' => 'http://img1.game-oldies.com/sites/default/files/packshots/sony-playstation/ctr-crash-team-racing.jpeg',
				'release_date' => '1999-12-16',
				'youtube_id_trailer' => 'GIQMlAazZds',
			],
			[
				'title'        => 'Gran Turismo 6',
				'slug'         => 'gran-turismo-6',
				'description'  => 'Gran Turismo 6 is de beste en mooiste racesimulatie voor de Playstation 3. Grafisch haalt het alles uit de kast, en met meer auto\'s dan ooit te voren worden racefans helemaal omver geblazen. Het racespel bevat meer dan twaalfhonderd auto\'s. Dat zijn er tweehonderd meer in vergelijking met het vijfde deel. Daarnaast worden er maandelijks nieuwe auto\'s aangeboden die via de online winkel van Playstation te koop zijn.',
				'price'        => 22.99,
				'genre_id'     => 1,
				'publisher_id' => 4,
				'platform_id'  => 3,
				'box_art_path' => 'http://s.s-bol.com/imgbase0/imagebase3/large/FC/8/2/4/4/9200000014054428.jpg',
				'release_date' => '2013-12-06',
				'youtube_id_trailer' => 'VbsZaa0ZZzA',
			],
		];
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		foreach ( $this->games as $game ) {
			Game::create( $game );
		}
	}
}
