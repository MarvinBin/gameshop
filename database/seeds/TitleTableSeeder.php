<?php

use App\Title;
use Illuminate\Database\Seeder;

class TitleTableSeeder extends Seeder {
	/**
	 * Array with default titles.
	 *
	 * @var array $titles
	 */
	protected $titles;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->titles = [
			[
				'text' => 'Mevrouw',
			],
			[
				'text' => 'De heer',
			]
		];
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		foreach ( $this->titles as $title ) {
			Title::create( $title );
		}
	}
}
