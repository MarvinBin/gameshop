<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Array with default users.
     *
     * @var array $users
     */
    protected $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = [
            [
                'title_id' => 2,
                'first_name' => 'Marvin',
                'Last_name' => 'Binneveld',
                'email' => 'm.m.binneveld@gmail.com',
                'password' => Hash::make('secret'),
                'date_of_birth' => '1992-07-27',
                'phone_number' => '0123-456789',
                'street_name' => 'Straatnaam',
                'house_number' => '1',
                'city' => 'Stad',
                'zip_code' => '1234 AB',
                'country' => 'Nederland',
                'is_admin' => 1,
                'is_shop_assistant' => 1
            ],
            [
                'title_id' => 2,
                'first_name' => 'Roy',
                'Last_name' => 'van Os',
                'email' => 'soultakercinemtatics@gmail.com',
                'password' => Hash::make('secret'),
                'date_of_birth' => '1992-07-27',
                'phone_number' => '0123-456789',
                'street_name' => 'Straatnaam',
                'house_number' => '1',
                'city' => 'Stad',
                'zip_code' => '1234 AB',
                'country' => 'Nederland',
                'is_admin' => 0,
                'is_shop_assistant' => 0
            ],
            [
                'title_id' => 2,
                'first_name' => 'Henk',
                'Last_name' => 'de Vries',
                'email' => 'henk@gmail.com',
                'password' => Hash::make('secret'),
                'date_of_birth' => '1992-07-27',
                'phone_number' => '0123-456789',
                'street_name' => 'Straatnaam',
                'house_number' => '1',
                'city' => 'Stad',
                'zip_code' => '1234 AB',
                'country' => 'Nederland',
                'is_admin' => 0,
                'is_shop_assistant' => 0
            ],
            [
                'title_id' => 1,
                'first_name' => 'Manon',
                'Last_name' => 'Bosvrouw',
                'email' => 'maja@gmail.com',
                'password' => Hash::make('secret'),
                'date_of_birth' => '1992-07-27',
                'phone_number' => '0123-456789',
                'street_name' => 'Straatnaam',
                'house_number' => '1',
                'city' => 'Stad',
                'zip_code' => '1234 AB',
                'country' => 'Nederland',
                'is_admin' => 0,
                'is_shop_assistant' => 0
            ],
            [
                'title_id' => 2,
                'first_name' => 'Pieter',
                'Last_name' => '\'t Veld',
                'email' => 'pieter@gmail.com',
                'password' => Hash::make('secret'),
                'date_of_birth' => '1992-07-27',
                'phone_number' => '0123-456789',
                'street_name' => 'Straatnaam',
                'house_number' => '1',
                'city' => 'Stad',
                'zip_code' => '1234 AB',
                'country' => 'Nederland',
                'is_admin' => 0,
                'is_shop_assistant' => 0
            ],
            [
                'title_id' => 2,
                'first_name' => 'Hans',
                'Last_name' => 'Achterveld',
                'email' => 'hans@gmail.com',
                'password' => Hash::make('secret'),
                'date_of_birth' => '1992-07-27',
                'phone_number' => '0123-456789',
                'street_name' => 'Straatnaam',
                'house_number' => '1',
                'city' => 'Stad',
                'zip_code' => '1234 AB',
                'country' => 'Nederland',
                'is_admin' => 0,
                'is_shop_assistant' => 0
            ],
            [
                'title_id' => 1,
                'first_name' => 'Natalia',
                'Last_name' => 'Stramnova',
                'email' => 'natalia@gmail.com',
                'password' => Hash::make('secret'),
                'date_of_birth' => '1992-07-27',
                'phone_number' => '0123-456789',
                'street_name' => 'Straatnaam',
                'house_number' => '1',
                'city' => 'Stad',
                'zip_code' => '1234 AB',
                'country' => 'Nederland',
                'is_admin' => 0,
                'is_shop_assistant' => 0
            ],
            [
                'title_id' => 1,
                'first_name' => 'Mikasa',
                'Last_name' => 'Jager',
                'email' => 'mikasa@gmail.com',
                'password' => Hash::make('secret'),
                'date_of_birth' => '1992-07-27',
                'phone_number' => '0123-456789',
                'street_name' => 'Straatnaam',
                'house_number' => '1',
                'city' => 'Stad',
                'zip_code' => '1234 AB',
                'country' => 'Nederland',
                'is_admin' => 0,
                'is_shop_assistant' => 0
            ],
            [
                'title_id' => 1,
                'first_name' => 'Marlena',
                'Last_name' => 'Wasink',
                'email' => 'marlena@gmail.com',
                'password' => Hash::make('secret'),
                'date_of_birth' => '1992-07-27',
                'phone_number' => '0123-456789',
                'street_name' => 'Straatnaam',
                'house_number' => '1',
                'city' => 'Stad',
                'zip_code' => '1234 AB',
                'country' => 'Nederland',
                'is_admin' => 0,
                'is_shop_assistant' => 0
            ],
            [
                'title_id' => 1,
                'first_name' => 'Nina',
                'Last_name' => 'van Nieuwpoort',
                'email' => 'nvn@gmail.com',
                'password' => Hash::make('secret'),
                'date_of_birth' => '1992-07-27',
                'phone_number' => '0123-456789',
                'street_name' => 'Straatnaam',
                'house_number' => '1',
                'city' => 'Stad',
                'zip_code' => '1234 AB',
                'country' => 'Nederland',
                'is_admin' => 0,
                'is_shop_assistant' => 0
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->users as $user) {
            User::create($user);
        }
    }
}
