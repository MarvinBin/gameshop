<?php

use App\Manufacturer;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ManufacturerTableSeeder extends Seeder {
	/**
	 * Array with default manufacturers.
	 *
	 * @var array $manufacturers
	 */
	protected $manufacturers;

	/**
	 * Constructor
	 *
	 * @param Faker $faker
	 */
	public function __construct( Faker $faker ) {
		$this->manufacturers = [
			[
				'name' => 'Sony',
				'slug' => 'sony',
				'description' => $faker->text(),
				'logo_path' => 'https://upload.wikimedia.org/wikipedia/commons/c/ca/Sony_logo.svg',
			],
			[
				'name' => 'Microsoft',
				'slug' => 'microsoft',
				'description' => $faker->text(),
				'logo_path' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Microsoft_logo_%282012%29.svg/1024px-Microsoft_logo_%282012%29.svg.png',
			],
		];
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		foreach ( $this->manufacturers as $manufacturer ) {
			Manufacturer::create( $manufacturer );
		}
	}
}
