<?php

use App\Genre;
use Illuminate\Database\Seeder;

class GenreTableSeeder extends Seeder {
	/**
	 * Array with default genres.
	 *
	 * @var array $genres
	 */
	protected $genres;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->genres = [
			[
				'name' => 'Race',
			],
		];
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		foreach ( $this->genres as $genre ) {
			Genre::create( $genre );
		}
	}
}
