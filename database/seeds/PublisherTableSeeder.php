<?php

use App\Publisher;
use Illuminate\Database\Seeder;

class PublisherTableSeeder extends Seeder {
	/**
	 * Array with default publishers.
	 *
	 * @var array $publishers
	 */
	protected $publishers;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->publishers = [
			[
				'name' => 'Bandai Namco',
				'slug' => 'bandai-namco',
				'description' => 'Lorem ipsum',
				'logo_path' => 'https://upload.wikimedia.org/wikipedia/en/6/66/Bandai_Namco_Entertainment_logo.jpg',
			],
			[
				'name' => 'EA',
				'slug' => 'ea',
				'description' => 'Lorem ipsum',
				'logo_path' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Electronic_Arts_logo.svg/180px-Electronic_Arts_logo.svg.png',
			],
			[
				'name' => 'Ubisoft',
				'slug' => 'ubisoft',
				'description' => 'Lorem ipsum',
				'logo_path' => 'https://upload.wikimedia.org/wikipedia/en/8/8c/Ubisoft.png',
			],
			[
				'name' => 'Sony Computer Entertainment',
				'slug' => 'sony-computer-entertainment',
				'description' => 'Lorem ipsum',
				'logo_path' => 'https://upload.wikimedia.org/wikipedia/en/thumb/4/47/Sony_Computer_Entertainment_logo.svg/200px-Sony_Computer_Entertainment_logo.svg.png',
			],
			[
				'name' => 'Empire Interactive',
				'slug' => 'empire-interactive',
				'description' => 'Lorem ipsum',
				'logo_path' => 'https://upload.wikimedia.org/wikipedia/en/1/1f/Empire_Interactive_Logo.png',
			],
			[
				'name' => 'Microsoft',
				'slug' => 'microsoft',
				'description' => 'Lorem ipsum',
				'logo_path' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Microsoft_logo_%282012%29.svg/1024px-Microsoft_logo_%282012%29.svg.png',
			],
			[
				'name' => 'Codemasters',
				'slug' => 'codemasters',
				'description' => 'Lorem ipsum',
				'logo_path' => 'https://upload.wikimedia.org/wikipedia/en/7/78/Codemasters-logo.png',
			],
		];
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		foreach ( $this->publishers as $publisher ) {
			Publisher::create( $publisher );
		}
	}
}
