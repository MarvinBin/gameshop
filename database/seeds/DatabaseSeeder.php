<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Array with table names.
	 *
	 * @var array $tables
	 */
	protected $tables;

	/**
	 * Array with seeder names.
	 *
	 * @var array $seeders .
	 */
	protected $seeders;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->tables = [
			'genres',
			'manufacturers',
			'publishers',
			'titles',
			'platforms',
			'games',
			'users',
			'game_reviews',
			'storage_locations'
		];

		$this->seeders = [
			'GenreTableSeeder',
			'ManufacturerTableSeeder',
			'PublisherTableSeeder',
			'TitleTableSeeder',
			'PlatformTableSeeder',
			'GameTableSeeder',
			'UserTableSeeder',
			// 'GameReviewTableSeeder',
			'StorageLocationTableSeeder'
		];
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
//		if ( App::environment( 'production' ) ) {
//			exit( 'Abort! This script can not run in a production environment because you will lose all data.' );
//		}

		Model::unguard();

		$this->truncateTables( $this->tables );

		$this->calls( $this->seeders );

		exit( 'Successfully seeded the database.' );
	}

	/**
	 * Truncate all the tables that are specified in the provided array.
	 *
	 * @param array $tables
	 *
	 * @return void
	 */
	private function truncateTables( $tables ) {
		DB::statement( 'SET FOREIGN_KEY_CHECKS=0;' );

		foreach ( $tables as $table ) {
			DB::table( $table )->truncate();
		}

		DB::statement( 'SET FOREIGN_KEY_CHECKS=1;' );
	}

	/**
	 * Call all the seeders that are specified in the provided array.
	 *
	 * @param array $seeders
	 */
	private function calls( $seeders ) {
		foreach ( $seeders as $seeder ) {
			$this->call( $seeder );
		}
	}
}
