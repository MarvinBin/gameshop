<?php

use Illuminate\Database\Seeder;

class StorageLocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1, 10) as $index) {
            \App\StorageLocation::create([
                'game_id' => $index,
                'sellable_quantity' => 10,
                'physical_quantity' => 10
            ]);
        }
    }
}
