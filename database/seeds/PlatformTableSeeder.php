<?php

use App\Platform;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class PlatformTableSeeder extends Seeder {
	/**
	 * Array with default platforms.
	 *
	 * @var array $platforms
	 */
	protected $platforms;

	/**
	 * Constructor
	 *
	 * @param Faker $faker
	 */
	public function __construct( Faker $faker ) {
		$this->platforms = [
			[
				'name'            => 'Playstation 1',
				'slug'            => 'playstation-1',
				'description'     => $faker->text(),
				'manufacturer_id' => 1,
			],
			[
				'name'            => 'Playstation 2',
				'slug'            => 'playstation-2',
				'description'     => $faker->text(),
				'manufacturer_id' => 1,
			],
			[
				'name'            => 'Playstation 3',
				'slug'            => 'playstation-3',
				'description'     => $faker->text(),
				'manufacturer_id' => 1,
			],
			[
				'name'            => 'Playstation 4',
				'slug'            => 'playstation-4',
				'description'     => $faker->text(),
				'manufacturer_id' => 1,
			],
			[
				'name'            => 'Xbox One',
				'slug'            => 'xbox-one',
				'description'     => $faker->text(),
				'manufacturer_id' => 2,
			],
		];
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		foreach ($this->platforms as $platform) {
			Platform::create($platform);
		}
	}
}
