

var gulp = require('gulp');
var elixir = require('laravel-elixir');
var autoprefixer = require('gulp-autoprefixer');
var nodemon = require('gulp-nodemon');

// gulp watch
elixir(function (mix) {
    'use strict';
    // SASS & Javascript compilation
    mix.sass('app.scss')
        .browserify('app.js')
        .browserify('admin.js');

    // Prefix CSS
    gulp.src('public/css/app.css')
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('public/css'));
});

// Moves fonts from node modules to public directory
gulp.task('move-fonts', function () {
    'use strict';

    gulp.src(['node_modules/font-awesome/fonts/*'])
        .pipe(gulp.dest('public/fonts'));
});

gulp.task('server', function () {
    'use strict';

    nodemon({
        script: 'resources/assets/js/server.js'
    });
});