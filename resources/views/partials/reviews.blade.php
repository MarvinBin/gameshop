<div class="row" ng-controller="GameReviewController" ng-init="init({{ $game->id }})">
    <div class="col-12">
        <h2>Reviews</h2>
        <p ng-hide="reviews.length">Er zijn nog geen reviews voor de game.</p>
        <a ng-click="toggleForm()" class="button mb-20">Schrijf een review</a>
        <div ng-show="formOpen" class="card card--review-form">
            <div class="card__body">
                <form ng-submit="submitReview()">
                    <div class="row">
                        <div class="col-12">
                            <label for="title">Beoordeling</label>
                            <i ng-click="setRating(1)" ng-class="{'yellow': review.rating >= 1}" class="fa fa-star star-1"></i>
                            <i ng-click="setRating(2)" ng-class="{'yellow': review.rating >= 2 }" class="fa fa-star star-2"></i>
                            <i ng-click="setRating(3)" ng-class="{'yellow': review.rating >= 3 }" class="fa fa-star star-3"></i>
                            <i ng-click="setRating(4)" ng-class="{'yellow': review.rating >= 4 }" class="fa fa-star star-4"></i>
                            <i ng-click="setRating(5)" ng-class="{'yellow': review.rating >= 5 }" class="fa fa-star star-5"></i>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="title">Titel van je review</label>
                            <input ng-model="review.title" name="title" id="title" type="text" placeholder="Vul hier de titel in"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="body">Beschrijf je ervaring met deze game</label>
                            <textarea ng-model="review.body" name="body" id="body" cols="30" rows="10" placeholder="Beschrijf hier je ervaring"></textarea>
                        </div>
                    </div>
                    <div class="row no-margin-bottom">
                        <div class="col-12">
                            <input class="button pull-right" type="submit" value="Verzenden"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div masonry
             item-selector=".card"
             masonry-options="{
                        gutter: 20,
                        transitionDuration: '0s'
                     }"
             load-images="false"
             ng-show="reviews.length"
             class="reviews"
                >
            <div masonry-brick ng-repeat="review in reviews | reverse" class="card card--review">
                <div class="card__body">
                    <i ng-class="{'yellow': review.stars >= 1}" class="fa fa-star star-1"></i>
                    <i ng-class="{'yellow': review.stars >= 2 }" class="fa fa-star star-2"></i>
                    <i ng-class="{'yellow': review.stars >= 3 }" class="fa fa-star star-3"></i>
                    <i ng-class="{'yellow': review.stars >= 4 }" class="fa fa-star star-4"></i>
                    <i ng-class="{'yellow': review.stars >= 5 }" class="fa fa-star star-5"></i>
                    <h3>@{{ review.title }}</h3>
                    <p>@{{ review.body }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>