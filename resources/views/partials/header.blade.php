<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="shortcut icon" href="images/gear-favicon.png" type="image/x-icon">
    <title>Gearbox Games</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.6/css/lightgallery.css"/>
</head>
<body ng-app="gameshop">
<div class="container">
    <header class="default-header">
        <div class="row no-margin-bottom">
            <div class="col-6">
                <img class="branding" src="/images/gearbox-games-logo.svg" alt="Gearbox Games logo"/>
            </div>
            <div class="col-6 no-margin-bottom">
                <a href="{{ route('shopping-cart.index') }}" class="button shopping-card-button pull-right"><i class="fa fa-shopping-cart"></i></a>
                <form ng-controller="SearchController" class="search-form pull-right">
                    <input ng-model="query" ng-change="loadResults()" ng-blur="blur()" ng-focus="focus()" type="text" placeholder="Zoek naar een game..."/>
                    <button class="button disabled"><i class="fa fa-search"></i></button>
                    <div ng-show="open" class="search-results is-hidden">
                        <section ng-repeat="result in searchResults" ng-show="result.data.length">
                            <div class="search-results__header">
                                @{{ result.title }}
                            </div>
                            <a ng-repeat="item in result.data" class="search-results__result" ng-href="@{{ generateUrl(item) }}">
                                @{{ item.title || item.name }} <br/>
                                <small>| @{{ item.platform.name || item.manufacturer.name }}</small>
                            </a>
                        </section>
                        <div ng-show="noResults" class="search-results__no-result">
                            Er zijn geen zoekresultaten
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <nav class="default-nav">
                    <a href="{{ route('home') }}">Home</a>
                    @foreach(\App\Platform::all() as $platform)
                        <a href="{{ route('platform.show-by-slug', ['platformSlug' => $platform->slug]) }}">{{ $platform->name }}</a>
                    @endforeach
                    {{--<div class="pull-right">--}}
                        @if(Auth::check())
                            <a href="{{ route('favorites.index') }}">Favorieten</a>
                            @if(Auth::user()->is_shop_assistant || Auth::user()->is_admin)
                            <a href="{{ route('admin.dashboard.index') }}">Admin</a>
                            @endif
                        <a href="{{ route('auth.logout') }}">Uitloggen</a>
                        @else
                        <a href="{{ route('auth.login.get') }}">Inloggen</a>
                        <a href="{{ route('auth.register.get') }}">Registreren</a>
                        @endif
                    {{--</div>--}}
                </nav>
            </div>
        </div>
    </header>
</div>