@if(! empty($platform) && ! $platform->games->isEmpty())
    <?php $games = $platform->games; ?>
@endif


@if(! empty($games) && ! $games->isEmpty())
    <div class="row games" ng-controller="GameController">
        <div class="modal" ng-show="modalOpen">
            <div ng-click="modalToggle()" class="modal__close-button">
                <i class="fa fa-times"></i>
            </div>
            <div class="modal__content">
                <div class="row">
                    <div class="col-4 center-text">
                        <img class="box-art-modal" ng-src="@{{ selectedGame.box_art_path }}"/>
                    </div>
                    <div class="col-8">
                        <h1>@{{ selectedGame.title }}</h1>
                        <p>@{{ selectedGame.description }}</p>
                        <div class="single-game__price-highlight" ng-class="{'blacked-out-price': selectedGame.discount_price}">€ @{{ selectedGame.price }}</div>
                        <div class="single-game__price-highlight" ng-if="selectedGame.discount_price">€ @{{ selectedGame.discount_price }}</div>
                        <div class="mb-20"></div>
                        <div class="form-group">
                            <label for="quantity">Aantal</label>
                            <input ng-model="quantity" name="quantity" id="quantity" type="number" min="1" max="10" step="1"/>
                            <p ng-show="quantityError">Het aantal moet tussen 1 en 10 liggen.</p>
                        </div>

                        <input ng-click="addToCart()" class="button" type="submit" ng-value="modalButtonText"/>
                    </div>
                </div>
            </div>
        </div>
        @foreach($games as $index => $game)
            <div class="card card--list card--game">
                @if (! $game->released)
                    <div class="card__label">Pre-order</div>
                @endif
                <div class="card__header center-text">
                    <img src="{{ $game->box_art_path }}" alt="Boxart {{ $game->title }}"/>
                </div>
                <div class="card__body center-text">
                    @foreach(range(1, 5) as $i)
                        <i class="fa fa-star
                                @if ($i <= $game->average_stars)
                                yellow
                                @endif
                                "></i>
                    @endforeach
                    <h3>
                        <a href="{{ route('game.show-by-slug', [
                                'platformSlug' => $game->platform->slug,
                                'gameSlug' => $game->slug
                            ]) }}">{{ $game->title }}</a>
                    </h3>
                    <p>{{ $game->description }}</p>
                    <p><strong>€ {{ $game->price }}</strong></p>
                    <p><small>{{ $game->publisher->name }} | {{ $game->platform->name }} | {{ $game->releaseYear }}</small></p>
                </div>
                <div class="card__footer no-padding-top">
                    <div class="row">
                        <div class="col-6">
                            <a ng-click="modalToggle({{ $game->id }})" class="button full-width"><i class="fa fa-cart-plus"></i></a>
                        </div>
                        <div class="col-6">
                            <a class="button full-width" ng-click="addToFavoritesListViaHome({{ $game->id }})">
                                <i class="fa fa-heart-o"></i>
                                <span id="game-added-{{ $game->id }}" class="is-hidden">Toegevoegd</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@else
    <div class="row">
        <div class="column">
            <strong>There are currently no games available for this platform.</strong>
            {{-- TODO: Make a component for message and swap this placeholder --}}
        </div>
    </div>
@endif