<div class="button back-to-top is-hidden"><i class="fa fa-chevron-up"></i></div>
<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.js"></script>
<script type="text/javascript" src="/js/app.js"></script>
@yield('script')
</body>
</html>