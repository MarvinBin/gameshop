@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Inloggen</h1>
                <p>Vul het onderstaande formulier in om in te loggen op Gearbox Games.</p>
                <form class="col-6" action="{{ route('auth.login.post') }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    @if (count($errors) > 0)
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <div class="form-group">
                        <label for="email">E-mailadres</label>
                        <input type="email" name="email" id="email" placeholder="Vul hier je e-mailadres in" value="{{ old('email') }}"/>
                    </div>

                    <div class="form-group">
                        <label for="password">Wachtwoord</label>
                        <input type="password" name="password" id="password" placeholder="Vul hier je wachthwoord in"/>
                    </div>

                    <input class="button pull-right" type="submit" value="Klik hier om in te loggen"/>
                </form>
            </div>
        </div>
    </div>
@stop