@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Registreren</h1>
                <p>Vul het onderstaande formulier in om een account te maken voor Gearbox Games.</p>
                <form class="col-6" action="{{ route('auth.register.post') }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    @if (count($errors) > 0)
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    
                    <div class="form-group">
                        <label for="title_id">Aanhef</label>
                        <select name="title_id" id="title_id">
                            @foreach(\App\Title::all() as $title)
                                <option value="{{ $title->id }}">{{ $title->text }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="first_name">Voornaam</label>
                        <input type="text" name="first_name" id="first_name" placeholder="Vul hier je voornaam in"/>
                    </div>

                    <div class="form-group">
                        <label for="last_name">Achternaam</label>
                        <input type="text" name="last_name" id="last_name" placeholder="Vul hier je achternaam in"/>
                    </div>

                    <div class="form-group">
                        <label for="email">E-mailadres</label>
                        <input type="email" name="email" id="email" placeholder="Vul hier je e-mailadres in" value="{{ old('email') }}"/>
                    </div>

                    <div class="form-group">
                        <label for="password">Wachtwoord</label>
                        <input type="password" name="password" id="password" placeholder="Vul hier je wachthwoord in"/>
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation">Bevestig wachtwoord</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Herhaal hier je wachtwoord">
                    </div>

                    <div class="form-group">
                        <label for="date_of_birth">Geboortedatum</label>
                        <input type="date" name="date_of_birth" id="date_of_birth"/>
                    </div>

                    <div class="form-group">
                        <label for="phone_number">(Mobiele) telefoonnummer</label>
                        <input type="text" name="phone_number" id="phone_number" placeholder="Vul hier je (mobiele) telefoonnummer in"/>
                    </div>

                    <div class="form-group">
                        <label for="street_name">Straatnaam</label>
                        <input type="text" name="street_name" id="street_name" placeholder="Vul hier je straatnaam in"/>
                    </div>

                    <div class="form-group">
                        <label for="house_number">Huisnummer</label>
                        <input type="text" name="house_number" id="house_number" placeholder="Vul hier je huisnummer in"/>
                    </div>

                    <div class="form-group">
                        <label for="zip_code">Postcode</label>
                        <input type="text" name="zip_code" id="zip_code" placeholder="Vul hier je postcode in"/>
                    </div>
                    
                    <div class="form-group">
                        <label for="country">Land</label>
                        <input type="text" name="country" id="country" placeholder="Vul hier je land in"/>
                    </div>

                    <input class="button pull-right" type="submit" value="Klik hier om je te registreren"/>
                </form>
            </div>
        </div>
    </div>

@stop