@extends('layouts.default')

@section('content')
    <div class="container" ng-controller="GameController">
        <div class="modal" ng-show="modalOpen">
            <div ng-click="modalToggle()" class="modal__close-button">
                <i class="fa fa-times"></i>
            </div>
            <div class="modal__content">
                <div class="row">
                    <div class="col-4 center-text">
                        <img class="box-art-modal" ng-src="@{{ selectedGame.box_art_path }}"/>
                    </div>
                    <div class="col-8">
                        <h1>@{{ selectedGame.title }}</h1>
                        <p>@{{ selectedGame.description }}</p>
                        <div class="single-game__price-highlight" ng-class="{'blacked-out-price': selectedGame.discount_price}">€ @{{ selectedGame.price }}</div>
                        <div class="single-game__price-highlight" ng-if="selectedGame.discount_price">€ @{{ selectedGame.discount_price }}</div>
                        <div class="mb-20"></div>
                        <div class="form-group">
                            <label for="quantity">Aantal</label>
                            <input ng-model="quantity" name="quantity" id="quantity" type="number" min="1" max="10" step="1"/>
                            <p ng-show="quantityError">Het aantal moet tussen 1 en 10 liggen.</p>
                        </div>

                        <input ng-click="addToCart()" class="button" type="submit" ng-value="modalButtonText"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h1>{{ $game->title }}</h1>
                <div class="divider"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-5">
                <div class="single-game__box-art-showcase">
                    <img src="{{ $game->box_art_path }}" alt="Box art {{ $game->title }}"/>
                </div>
            </div>
            <div class="col-7">
                <h2>Description</h2>
                <p>{{ $game->description }}</p>
                <div class="single-game__price-highlight
                 @if(! $game->released)
                    blacked-out-price
                 @endif
                ">€ {{ $game->price }}</div>
                @if(! $game->released)
                    <div class="single-game__price-highlight">€ {{ $game->discounted_price }}</div>
                    <div>Pre-order korting: {{ $game->discount_percentage }}%</div>
                @endif
                <br/>
                <a ng-click="modalToggle({{ $game->id }})" class="button"><i class="fa fa-cart-plus"></i> Add to shopping cart</a>
                <a class="button" ng-click="addToFavoritesList({{ $game->id }})"><i class="fa fa-heart-o"></i> @{{ favoriteListButtonText }}</a>
                @if(! $game->released)
                    <p class="single-game__not-released-yet">This game isn't released yet, it will be available on {{ $game->release_date->format('d-m-Y') }}.
                        <br/>When you pre-order it, you can pick up the game in the store on the release date.</p>
                @endif
            </div>
        </div>
        <div class="row" >
            <div class="col-6" ng-controller="GameScreenshotController" ng-init="init({{ $game->id }})">
                <h2>Screenshots</h2>
                <div masonry
                     item-selector=".single-game__screenshot"
                     masonry-options="{
                        gutter: 20,
                        transitionDuration: '0s'
                     }"
                     load-images="false" ng-show="screenshots.length"
                     id="lightgallery"
                     class="screenshots"
                >
                    <a masonry-brick emit-repeat-finished ng-repeat="screenshot in screenshots" class="single-game__screenshot" ng-href="@{{ screenshot.image_path }}">
                        <img ng-src="@{{ screenshot.image_path }}"/>
                    </a>
                </div>
                <p ng-hide="screenshots.length">Er zijn geen screenshots voor deze game.</p>
                <h2>Specification</h2>
                <ul>
                    <li>Genre: {{ $game->genre->name }}</li>
                    <li>Platform: <a href="{{ route('platform.show-by-slug', ['platformSlug' => $game->platform->slug]) }}">{{ $game->platform->name }}</a></li>
                    <li>Publisher: {{ $game->publisher->name }}</li>
                    <li>Release date: {{ $game->release_date->format('d-m-Y') }}</li>
                </ul>
            </div>
            <div class="col-6">
                <h2>Trailer</h2>
                @if($game->youtube_id_trailer)
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/{{ $game->youtube_id_trailer }}" frameborder="0" allowfullscreen></iframe>
                @else
                    <p>Er is geen trailer voor deze game.</p>
                @endif
            </div>
        </div>
        @include('partials.reviews')
@stop