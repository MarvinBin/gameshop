@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1>Favorieten</h1>
            @include('partials.games')
        </div>
    </div>
@stop