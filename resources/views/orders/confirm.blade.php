@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Bevestig uw bestelling</h1>
                <div class="row">
                    <div class="col-6">
                        <h2>Uw gegevens</h2>
                        <strong>Naam:</strong> {{ Auth::user()->first_name }} {{ Auth::user()->last_name }} <br/>
                        <strong>E-mailadres:</strong> {{ Auth::user()->email }}
                    </div>
                    <div class="col-6">
                        <h2>Betaalmethode</h2>
                        <strong>{{ Request::session()->get('payment_method') }}</strong>
                    </div>
                </div>

                <form action="{{ route('orders.confirm.post')}}" method="POST">
                    <input class="button pull-right" type="submit" value="Klik hier om uw bestelling te bevestigen"/>
                </form>
            </div>
        </div>
    </div>
@stop