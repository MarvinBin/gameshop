@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="col-12">
            <h1>Bedankt voor uw bestelling</h1>
            <p>U zal binnekort een e-mail van ons ontvangen met daarin een orderbewijs. Dit orderbewijs bevat een barcode waarmee u de door u bestelde game op kunt halen in onze winkel.</p>
            <p><strong>Let op:</strong> De barcodes voor pre-orders worden pas verzonden op de dag van de release.</p>
        </div>
    </div>
@stop


