@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12" ng-controller="ShoppingCartController">
                <h1>Winkelwagen</h1>
                <p ng-hide="contents.length">U heeft geen games in uw winkelwagen zitten.</p>
                <div emit-repeat-finished ng-repeat="item in contents" class="row shopping-cart__item">
                    <div class="col-2">
                        <img ng-src="@{{ item.attributes.box_art }}"/>
                    </div>
                   <div class="col-3">
                       <strong>@{{ item.name }}</strong>
                       <br/>
                       <small>@{{ item.attributes.platform_name }}</small>
                   </div>
                    <div class="col-2">
                        Aantal: <input ng-model="item.quantity" ng-change="updateQuantity(item)" type="number" min="1" max="10" step="1" style="width: 30%"/>
                    </div>
                    <div class="col-2">
                        @{{ item.price | currency : '€ ' }}
                    </div>
                    <div class="col-2">
                        @{{ item.price * item.quantity | currency : '€ ' }}
                    </div>
                    <div class="col-1">
                        <a class="primary-color" ng-click="removeFromCart(item)" href="#"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="pull-right">
                        <strong>Totaal:</strong> @{{ total  | currency : '€ '}}
                    </div>
                </div>
                @if(Auth::check())
                    <a class="button pull-right" href="{{ route('payment.choose-method') }}">Doorgaan naar de betaling</a>
                @else
                    <p class="pull-right">U moet ingelogd zijn om te bestellen.</p>
                @endif
            </div>
        </div>
    </div>
@stop