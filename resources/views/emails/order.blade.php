Beste {{ $user->title->text }} {{ $user->first_name }} {{ $user->last_name }},
<br/><br/>
Bedankt voor uw bestelling bij Gearbox Games. Bij deze e-mail vindt u als bijlage een pdf. Met deze pdf kan u de door u bestelde games op halen in de winkel.
