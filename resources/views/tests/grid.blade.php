<div class="container container--with-padding">
    <div class="row">
        <div class="column">1</div>
    </div>
    @foreach(range(2, $columns = 12) as $column)
    <div class="row">
        @foreach(range(1, $column) as $el)
        <div class="column column--{{ $column }}">{{ $column }}</div>
        @endforeach
    </div>
    @endforeach
</div>