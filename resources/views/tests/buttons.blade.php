<div class="container container--with-padding">
    <div class="row">
        <div class="column">
            <h1>Lorem Ipsum</h1>

            <p>Maxime taciti massa rem, soluta eaque risus perspiciatis? Torquent dictum aliquid luctus, donec feugiat
                dui doloremque ex blandit possimus per, itaque lorem distinctio excepteur? Impedit cras inventore. Amet
                imperdiet unde. Adipisicing eaque tortor? Aliquam hac! Magni, cupiditate justo pede aenean.</p>
        </div>
    </div>
    <div class="row">
        @foreach (range(1, 3) as $element)
        <div class="column column--3">
            <div class="card card--list">
                @if (rand(0, 1))
                <div class="card__label">Pre-order</div>
                @endif
                <div class="card__header center-text">
                    <img src="http://s.s-bol.com/imgbase0/imagebase3/large/FC/1/9/6/7/9200000045947691.jpg" alt="Boxart Project Cars"/>
                </div>
                <div class="card__body center-text">
                    <?php $rating = rand(3, 5); ?>
                    @foreach(range(1, 5) as $i)
                    <i class="fa fa-star
                    @if ($i < $rating)
                    yellow
                    @endif
                    "></i>
                    @endforeach
                    <h3>Need for speed: 2015</h3>
                    <p>Debitis veritatis primis viverra pharetra.</p>
                    <p>€ 39,-</p>
                    <p><small>EA | Playstation 4 | 2015</small></p>
                </div>
                <div class="card__footer no-padding-top">
                    <div class="row">
                        <div class="column column--2 column--2--small">
                            <a class="button full-width"><i class="fa fa-cart-plus"></i></a>
                        </div>
                        <div class="column column--2 column--2--small">
                            <a class="button full-width"><i class="fa fa-heart-o"></i></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>