<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin | Gearbox Games</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body ng-app="gameshop-admin">
    <header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ route('admin.dashboard.index') }}">admin<strong>panel</strong></a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ route('admin.dashboard.index') }}">Home</a></li>
                        <li><a href="{{ route('admin.day-overview') }}">Day Overview</a></li>
                        <li><a href="{{ route('admin.pre-orders') }}">Pre-orders</a></li>
                        @if(Auth::user()->is_admin)
                        <li><a href="{{ route('admin.games.index') }}">Games</a></li>
                        <li><a href="{{ route('admin.publishers.index') }}">Publishers</a></li>
                        <li><a href="{{ route('admin.platforms.index') }}">Platforms</a></li>
                        <li><a href="{{ route('admin.manufacturers.index') }}">Manufacturers</a></li>
                        <li><a href="{{ route('admin.users.index') }}">Users</a></li>
                        <!-- <li><a href="{{ route('admin.orders.index') }}">Orders</a></li> -->
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div class="container">
        @yield('content')
    </div>

<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
<script src="{{ asset('js/admin.js') }}"></script>
@yield('script')
</body>
</html>