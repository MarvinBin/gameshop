@extends('admin.layouts.default')

@section('content')
    <div class="row" ng-controller="BarcodeReaderController">
        <h1>Dashboard</h1>
        <div class="form-group">
            <input ng-model="barcode" ng-change="checkForOrder()" class="form-control" type="text" placeholder="Barcode"/>
        </div>
        <div ng-show="error" class="alert alert-danger" role="alert">
            @{{ error }}
        </div>
        <div class="order" ng-show="order.id">
            <h2>Order #@{{ order.id }}</h2>
            <strong>Name: </strong> @{{ order.user.first_name }} @{{ order.user.last_name }} <br/>
            <strong>Paid:</strong> <input type="checkbox" ng-model="order.paid" ng-disabled="order.paid"/>

            <table class="table" ng-show="orderRules.length">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Storage Location(s)</th>
                        <th>Quantity</th>
                        <th>Retrieved</th>
                        <th>Info</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="orderRule in orderRules" ng-class="{'danger': orderRule.game.notReleased}">
                        <td>@{{ orderRule.game.title }}</td>
                        <td>
                            <div ng-repeat="location in orderRule.game.storage_locations">@{{ location.id }}</div>
                        </td>
                        <td>@{{ orderRule.quantity }}</td>
                        <td><input type="checkbox" ng-model="orderRule.retrieved" ng-disabled="orderRule.retrieved || orderRule.game.notReleased" ng-change="setRetrieved(orderRule)"/></td>
                        <td>
                            <span ng-show="orderRule.game.notReleased">Deze game is nog niet gereleased.</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop