@extends('admin.layouts.default')

@section('content')
    <h1>Day Overview</h1>
    <h2><strong>Omzet:</strong> € {{ $profit }}</h2>

    @foreach($orders as $order)
        <h3>Order {{ $order->id }}</h3>
        <table class="table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Pre-order</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->orderRules as $rule)
                    <tr>
                        <td>{{ $rule->game->title }}</td>
                        <td>{{ $rule->quantity }}</td>
                        <td>€ {{ $rule->game->price }}</td>
                        <td>{{ (! $rule->game->released) ? 'Ja' : 'Nee' }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endforeach
@stop