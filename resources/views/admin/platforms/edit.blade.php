@extends('admin.layouts.default')

@section('content')
    <h1>Update Platform</h1>
    <form action="{{ route('admin.platforms.update', ['platforms' => $platform->id]) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="title">Name</label>
            <input class="form-control" name="name" id="name" type="text" placeholder="Enter name" value="{{ $platform->name }}"/>
        </div>

        <div class="form-group">
            <label for="slug">Slug</label>
            <input class="form-control" name="slug" id="slug" type="text" placeholder="Enter slug" value="{{ $platform->slug }}"/>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" id="description" placeholder="Enter description" rows="3">{{ $platform->description }}</textarea>
        </div>
        <div class="form-group">
            <label for="manufacturer">Manufacturer</label>
            <select class="form-control" name="manufacturer" id="manufacturer">
                <option value="0">Choose manufacturer</option>
                @foreach($manufacturers as $manufacturer)
                    <option value="{{ $manufacturer->id }}"
                            @if ($manufacturer->id === $platform->manufacturer->id)
                                selected
                            @endif
                    >{{ $manufacturer->name }}</option>
                @endforeach
            </select>
        </div>

        <input type="submit" class="btn btn-primary pull-right" value="Save Platform"/>
    </form>
    <br/><br/>
@stop