@extends('admin.layouts.default')

@section('content')
    <a href="{{ route('admin.platforms.create') }}" class="btn btn-primary pull-right">Add new platform</a>
    <h1>Platforms</h1>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach($platforms as $platform)
            <tr id="platform-{{ $platform->id }}">
                <td>{{ $platform->id }}</td>
                <td>{{ $platform->name }}</td>
                <td>{{ $platform->manufacturer->name }}</td>
                <td><a href="{{ route('admin.platforms.edit', ['platforms' => $platform->id]) }}"><i class="fa fa-pencil"></i></a></td>
                <td><a href="#" onClick="deletePlatform({{ $platform->id }})"><i class="fa fa-trash"></i></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop

@section('script')
    <script>
        // TODO: REFACTOR FUNCTION FROM SPECIFIC TO GENERIC FOR EVERY RESOURCE IN THE ADMIN PANEL
        function deletePlatform(id) {
            if (confirm('Are you sure you want to delete this game?')) {
                $.ajax({
                    url: '/admin/platforms/' + id,
                    method: 'DELETE',
                    success: function(response) {
                        console.log(response);
                        $('#platform-'+id).remove();
                    }
                });
            }
        }
    </script>
@stop
@stop