@extends('admin.layouts.default')

@section('content')
    <h1>New Platform</h1>
    <form action="{{ route('admin.platforms.store') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="title">Name</label>
            <input class="form-control" name="name" id="name" type="text" placeholder="Enter name"/>
        </div>

        <div class="form-group">
            <label for="slug">Slug</label>
            <input class="form-control" name="slug" id="slug" type="text" placeholder="Enter slug"/>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" id="description" placeholder="Enter description" rows="3"></textarea>
        </div>

        <div class="form-group">
            <label for="manufacturer">Manufacturer</label>
            <select class="form-control" name="manufacturer" id="manufacturer">
                <option value="0">Choose manufacturer</option>
                @foreach($manufacturers as $manufacturer)
                    <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
                @endforeach
            </select>
        </div>

        <input type="submit" class="btn btn-primary pull-right" value="Save Platform"/>
    </form>
    <br/><br/>
@stop