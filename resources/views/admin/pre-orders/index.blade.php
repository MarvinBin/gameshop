@extends('admin.layouts.default')

@section('content')
  {{ $kortingen }}
    <h1>Pre-orders12</h1>
    <table class="table">
        <thead>
            <tr>
                <th>Korting</th>
                <th>Openstaand (€)</th>
                <th>Korting (€)</th>
            </tr>
        </thead>
        <tbody>
        @foreach($discounts as $discount)
            <tr>
                {{ $discount }}
            </tr>
        @endforeach
        </tbody>
    </table>
@stop
