@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <h1>Pre-orders</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>Korting</th>
                    <th>Openstaand (€)</th>
                    <th>Korting (€)</th>
                </tr>
            </thead>
            <tbody>
                @foreach($discounts as $key => $discount)
                    <tr>
                        <td>{{ $key }}%</td>
                        <td>{{ money_format('%i', $discount['openstaand']) }}</td>
                        <td>{{ money_format('%i',$discount['korting']) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@stop