@extends('admin.layouts.default')

@section('content')
    <h1>Update Game</h1>
    <form action="{{ route('admin.games.update', ['games' => $game->id]) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="title">Title</label>
            <input class="form-control" name="title" id="title" type="text" placeholder="Enter title" value="{{ $game->title }}"/>
        </div>

        <div class="form-group">
            <label for="slug">Slug</label>
            <input class="form-control" name="slug" id="slug" type="text" placeholder="Enter slug" value="{{ $game->slug }}"/>
        </div>

        <div class="form-group">
            <label for="path-to-box-art">Url to box art</label>
            <input class="form-control" name="path-to-box-art" id="path-to-box-art" type="text" placeholder="Enter url to box art" value="{{ $game->box_art_path }}"/>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" id="description" placeholder="Enter description" rows="3">{{ $game->description }}</textarea>
        </div>

        <div class="form-group">
            <label for="youtube-id-trailer">Youtube id for trailer</label>
            <input class="form-control" name="youtube-id-trailer" id="youtube-id-trailer" type="text" placeholder="Enter Youtube id for trailer" value="{{ $game->youtube_id_trailer }}"/>
        </div>

        <div class="form-group">
            <label for="price">Price</label>
            <input class="form-control" name="price" id="price" type="number" step="0.01" placeholder="Enter price" value="{{ $game->price }}"/>
        </div>

        <div class="form-group">
            <label for="release-date">Release date</label>
            <input class="form-control" name="release-date" id="release-date" type="date" placeholder="Enter release date" value="{{ $game->release_date->format('Y-m-d') }}"/>
        </div>

        <div class="form-group">
            <label for="genre">Genre</label>
            <select class="form-control" name="genre" id="genre">
                <option value="0">Choose genre</option>
                @foreach($genres as $genre)
                    <option value="{{ $genre->id }}"
                        @if ($genre->id === $game->genre->id)
                            selected
                        @endif
                    >{{ $genre->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="publisher">Publisher</label>
            <select class="form-control" name="publisher" id="publisher">
                <option value="0">Choose publisher</option>
                @foreach($publishers as $publisher)
                    <option value="{{ $publisher->id }}"
                            @if ($publisher->id === $game->publisher->id)
                                selected
                            @endif
                    >{{ $publisher->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="platform">Platform</label>
            <select class="form-control" name="platform" id="platform">
                <option value="0">Choose platform</option>
                @foreach($platforms as $platform)
                    <option value="{{ $platform->id }}"
                        @if ($platform->id === $game->platform->id)
                            selected
                        @endif
                    >{{ $platform->name }}</option>
                @endforeach
            </select>
        </div>

        <input type="submit" class="btn btn-primary pull-right" value="Save Game"/>
    </form>
    <br/><br/>
@stop