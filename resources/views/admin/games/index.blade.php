@extends('admin.layouts.default')

@section('content')
<a href="{{ route('admin.games.create') }}" class="btn btn-primary pull-right">Add new game</a>
<h1>Games</h1>
<table class="table">
  <thead>
   <tr>
    <th>ID</th>
    <th>Title</th>
    <th>Platform</th>
    <th>Publisher</th>
    <th>Edit</th>
    <th>Delete</th>
   </tr>
  </thead>
 <tbody>
  @foreach($games as $game)
   <tr id="game-{{ $game->id }}">
     <td>{{ $game->id }}</td>
     <td>{{ $game->title }}</td>
     <td>{{ $game->platform->name }}</td>
     <td>{{ $game->publisher->name }}</td>
     <td><a href="{{ route('admin.games.edit', ['games' => $game->id]) }}"><i class="fa fa-pencil"></i></a></td>
     <td><a href="#" onClick="deleteGame({{ $game->id }})"><i class="fa fa-trash"></i></a></td>
   </tr>
  @endforeach
 </tbody>
</table>
@stop

@section('script')
<script>
    function deleteGame(id) {
        if (confirm('Are you sure you want to delete this game?')) {
            $.ajax({
                url: '/admin/games/' + id,
                method: 'DELETE',
                success: function(response) {
                    console.log(response);
                    $('#game-'+id).remove();
                }
            });
        }
    }
</script>
@stop