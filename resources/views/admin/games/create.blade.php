@extends('admin.layouts.default')

@section('content')
    <h1>New Game</h1>
    <form action="{{ route('admin.games.store') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="title">Title</label>
            <input class="form-control" name="title" id="title" type="text" placeholder="Enter title"/>
        </div>

        <div class="form-group">
            <label for="slug">Slug</label>
            <input class="form-control" name="slug" id="slug" type="text" placeholder="Enter slug"/>
        </div>

        <div class="form-group">
            <label for="path-to-box-art">Url to box art</label>
            <input class="form-control" name="path-to-box-art" id="path-to-box-art" type="text" placeholder="Enter url to box art"/>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" id="description" placeholder="Enter description" rows="3"></textarea>
        </div>

        <div class="form-group">
            <label for="youtube-id-trailer">Youtube id for trailer</label>
            <input class="form-control" name="youtube-id-trailer" id="youtube-id-trailer" type="text" placeholder="Enter Youtube id for trailer"/>
        </div>

        <div class="form-group">
            <label for="price">Price</label>
            <input class="form-control" name="price" id="price" type="number" step="0.01" placeholder="Enter price"/>
        </div>

        <div class="form-group">
            <label for="release-date">Release date</label>
            <input class="form-control" name="release-date" id="release-date" type="date" placeholder="Enter release date"/>
        </div>

        <div class="form-group">
            <label for="genre">Genre</label>
            <select class="form-control" name="genre" id="genre">
                <option value="0">Choose genre</option>
                @foreach($genres as $genre)
                    <option value="{{ $genre->id }}">{{ $genre->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="publisher">Publisher</label>
            <select class="form-control" name="publisher" id="publisher">
                <option value="0">Choose publisher</option>
                @foreach($publishers as $publisher)
                    <option value="{{ $publisher->id }}">{{ $publisher->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="platform">Platform</label>
            <select class="form-control" name="platform" id="platform">
                <option value="0">Choose platform</option>
                @foreach($platforms as $platform)
                    <option value="{{ $platform->id }}">{{ $platform->name }}</option>
                @endforeach
            </select>
        </div>

        <input type="submit" class="btn btn-primary pull-right" value="Save Game"/>
    </form>
    <br/><br/>
@stop