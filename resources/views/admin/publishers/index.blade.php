@extends('admin.layouts.default')

@section('content')
    <a href="{{ route('admin.publishers.create') }}" class="btn btn-primary pull-right">Add new publisher</a>
    <h1>Publishers</h1>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach($publishers as $publisher)
            <tr id="publisher-{{ $publisher->id }}">
                <td>{{ $publisher->id }}</td>
                <td>{{ $publisher->name }}</td>
                <td><a href="{{ route('admin.publishers.edit', ['publishers' => $publisher->id]) }}"><i class="fa fa-pencil"></i></a></td>
                <td><a href="#" onClick="deletePublisher({{ $publisher->id }})"><i class="fa fa-trash"></i></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop

@section('script')
    <script>
        function deletePublisher(id) {
            if (confirm('Are you sure you want to delete this game?')) {
                $.ajax({
                    url: '/admin/publishers/' + id,
                    method: 'DELETE',
                    success: function(response) {
                        console.log(response);
                        $('#publisher-'+id).remove();
                    }
                });
            }
        }
    </script>
@stop
@stop