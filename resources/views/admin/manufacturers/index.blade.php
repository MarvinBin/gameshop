@extends('admin.layouts.default')

@section('content')
    <a href="{{ route('admin.manufacturers.create') }}" class="btn btn-primary pull-right">Add new manufacturer</a>
    <h1>Manufacturers</h1>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach($manufacturers as $manufacturer)
            <tr id="manufacturer-{{ $manufacturer->id }}">
                <td>{{ $manufacturer->id }}</td>
                <td>{{ $manufacturer->name }}</td>
                <td><a href="{{ route('admin.manufacturers.edit', ['manufacturers' => $manufacturer->id]) }}"><i class="fa fa-pencil"></i></a></td>
                <td><a href="#" onClick="deleteManufacturer({{ $manufacturer->id }})"><i class="fa fa-trash"></i></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop

@section('script')
    <script>
        // TODO: REFACTOR FUNCTION FROM SPECIFIC TO GENERIC FOR EVERY RESOURCE IN THE ADMIN PANEL
        function deleteManufacturer(id) {
            if (confirm('Are you sure you want to delete this game?')) {
                $.ajax({
                    url: '/admin/manufacturers/' + id,
                    method: 'DELETE',
                    success: function(response) {
                        console.log(response);
                        $('#manufacturer-'+id).remove();
                    }
                });
            }
        }
    </script>
@stop
@stop