@extends('admin.layouts.default')

@section('content')
    <h1>Update Manufacturer</h1>
    <form action="{{ route('admin.manufacturers.update', ['manufacturers' => $manufacturer->id]) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="title">Name</label>
            <input class="form-control" name="name" id="name" type="text" placeholder="Enter name" value="{{ $manufacturer->name }}"/>
        </div>

        <div class="form-group">
            <label for="slug">Slug</label>
            <input class="form-control" name="slug" id="slug" type="text" placeholder="Enter slug" value="{{ $manufacturer->slug }}"/>
        </div>

        <div class="form-group">
            <label for="logo-path">Url to logo</label>
            <input class="form-control" name="logo-path" id="logo-path" type="text" placeholder="Enter url to logo" value="{{ $manufacturer->logo_path }}"/>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" id="description" placeholder="Enter description" rows="3">{{ $manufacturer->description }}</textarea>
        </div>

        <input type="submit" class="btn btn-primary pull-right" value="Save Manufacturer"/>
    </form>
    <br/><br/>
@stop