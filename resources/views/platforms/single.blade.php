@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="column">
                <h1>{{ $platform->name }}</h1>
                <p>{{ $platform->description }}</p>
            </div>
        </div>
        @include('partials.games')
    </div>
@stop