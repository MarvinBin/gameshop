@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Kies uw gewenste betaalmethode</h1>
                <p>Kies met welke methode uw de games wilt betalen. Na het succesvol afronden van de betaling wordt er een orderbewijs met barcode naar uw e-mailadres toegestuurd. Met dit orderbewijs kunt u de games in de winkel ophalen.</p>
                <form action="{{ route('payment.save-method') }}" method="POST">
                    <label for="payment_method">Betaalmethode</label>
                    @foreach($paymentMethods as $key => $method)
                        <input type="radio" name="payment_method" id="payment_method" value="{{ $method }}" required/> {{ $method }}
                    @endforeach
                    <input class="button pull-right" type="submit" value="Ga verder"/>
                </form>
            </div>
        </div>
    </div>
@stop