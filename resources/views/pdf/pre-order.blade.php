<style>
    .barcode {
        text-align: center;
    }

    .barcode div {
        display: inline-block;
    }

    table {
        width: 100%;
    }

    table thead tr th {
        text-align: left;
        border-bottom: 1px solid #ddd;
    }

    h2 {
        width: 100%;
        background-color: #2ECC71;
        display: inline-block;
        padding: 5px 10px;
        color: #fff;
    }

    .center-text {
        text-align: center;
    }
</style>

<h1 class="center-text">Factuur Gearbox Games</h1>
<h2>Klant</h2>
{{ $order->user->title->text }} {{ $order->user->first_name }} {{ $order->user->last_name }} <br/>
{{ $order->user->street_name }} {{ $order->user->house_number }} <br/>
{{ $order->user->zip_code }} {{ $order->user->city }} <br/>
{{ $order->user->country }}
<br/>
<h2>Game</h2>
<table>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Prijs p.s.</th>
            <th>Aantal</th>
            <th>Totaal</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                {{ $rule->game->title }} <br/>
                <small>| {{ $rule->game->platform->name }}</small>
            </td>
            <td>
                € {{ $rule->game->discounted_price }}
            </td>
            <td>
                {{ $rule->quantity }}
            </td>
            <td>
                € {{ $rule->game->discount_price * $rule->quantity }}
            </td>
        </tr>
    </tbody>
</table>

<h2>Barcode</h2>

<div class="barcode">
    {!! $barcode !!}
</div>
