var angular = require('angular');

(function (ng) {
    'use strict';

    var gameshopAdmin = ng.module('gameshop-admin', []);

    gameshopAdmin.controller('BarcodeReaderController', function ($scope, $http) {
        $scope.barcode = '';
        $scope.order = {};
        $scope.orderRules = [];
        $scope.error = '';

        $scope.checkForOrder = function () {
            $http.get('/api/v1/orders/' + $scope.barcode)
                .success(function (response) {
                    $scope.error = '';
                    $scope.order = response;

                    if ($scope.order.paid === "1") {
                        $scope.order.paid = true;
                    } else {
                        $scope.order.paid = false;
                    }

                    $http.get('/api/v1/order-rules/order/' + $scope.order.id)
                        .success(function (response) {
                            $scope.orderRules = response;

                            $scope.orderRules.forEach(function (rule) {
                                if (rule.retrieved === "1") {
                                    rule.retrieved = true;
                                } else {
                                    rule.retrieved = false;
                                }

                                $http.get('/api/v1/games/check-if-released/' + rule.game.id)
                                    .success(function (response) {
                                        if (! response) {
                                            rule.game.notReleased = true;
                                        }
                                    });
                            });
                        });
                })
                .error(function () {
                    $scope.order = {};
                    $scope.error = 'Er is geen order voor deze barcode.';
                });
        };

        $scope.setRetrieved =  function (orderRule) {
          $http.post('/api/v1/order-rules/' + orderRule.id + '/set-retrieved', {
              retrieved: orderRule.retrieved
          }).success(function (response) {
              console.log(response);
          });
        };
    });
})(angular);