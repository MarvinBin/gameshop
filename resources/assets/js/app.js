var angular = require('angular');
var angularMasonry = require('angular-masonry');
var lightGallery = require('lightgallery');

(function ($, ng, ngMasonry, lightGallery) {
    'use strict';

    $('.games').masonry({
        itemSelector: '.card',
        gutter: 20
    });

    var gameshop = ng.module('gameshop', ['wu.masonry']);

    // Custom filter to reverse the order of elements in an array
    gameshop.filter('reverse', function () {
        return function (items) {
            return items.slice().reverse();
        };
    });

    // Custom directive to check if ng-repeat is finished
    gameshop.directive('emitRepeatFinished', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit('ngRepeatFinished');
                    });
                }
            }
        };
    });

    // Search Controller
    gameshop.controller('SearchController', function ($scope, $http, $timeout) {
        $scope.query = '';
        $scope.searchResults = [];
        $scope.open = false;
        $scope.noResults = true;

        $scope.blur = function () {
            $timeout(function () {
                $scope.open = false;
            }, 100);
        };

        $scope.focus = function () {
            if ($scope.query.length) {
                $scope.open = true;
            }
        };

        $scope.generateUrl = function (item) {
            if (item.title) {
                return '/' + item.platform.slug + '/' + item.slug;
            }

            if (item.name) {
                return '/' + item.slug;
            }
        };

        $scope.loadResults = function () {
            $scope.open = true;

            $http.post('/api/v1/search', {
                query: $scope.query
            }).success(function (response) {
                console.log(response);
                $scope.noResults = false;
                $scope.searchResults = response;
            }).error(function () {
                $scope.searchResults = [];
                $scope.noResults = true;
            });
        };

        $('.search-results').removeClass('is-hidden');
    });

    // Shopping Cart Controller
    gameshop.controller('ShoppingCartController', function ($scope, $http) {
        $scope.contents = [];
        $scope.total = 0;

        $http.get('/api/v1/shopping-cart/get-contents')
            .success(function (response) {
                $scope.contents = response;

                $scope.contents.forEach(function (item) {
                    $http.get('/api/v1/games/check-if-released/' + item.id)
                        .success(function (response) {
                            if (! response) {
                                $http.get('/api/v1/games/discount/' + item.id)
                                    .success(function (response) {
                                        item.price = response;
                                        $scope.calculateTotal();
                                    });
                            }
                        });
                });
            });

        $scope.removeFromCart = function (item) {
            $http.delete('/api/v1/shopping-cart/remove-game/' + item.id)
                .success(function () {
                    $scope.contents.splice($scope.contents.indexOf(item), 1);
                    $scope.calculateTotal();
                    $scope.apply();
                });
        };

        $scope.updateQuantity = function (item) {
            $http.post('/api/v1/shopping-cart/update-quantity/' + item.id, {
                quantity: item.quantity
            }).success(function () {
                $scope.calculateTotal();
            });
        };

        $scope.calculateTotal = function () {
            $scope.total = 0;

            $scope.contents.forEach(function (item) {
                $scope.total += item.price * item.quantity;
            });
        };

        $scope.$on('ngRepeatFinished', function () {
            $scope.calculateTotal();
        });
    });

    // Game Controller
    gameshop.controller('GameController', function ($scope, $http, $timeout) {

        $scope.modalOpen = false;
        $scope.selectedGame = {};
        $scope.quantity = 1;
        $scope.modalButtonText = 'Voeg toe aan winkelwagen';
        $scope.quantityError = false;
        $scope.favoriteListButtonText = 'Voeg toe aan favorietenlijst';

        $scope.modalToggle = function (gameId) {
            $scope.modalOpen = !$scope.modalOpen;

            if ($scope.modalOpen) {

                $http.get('/api/v1/games/' + gameId)
                    .success(function (response) {
                        $scope.selectedGame = response;

                        $http.get('/api/v1/games/check-if-released/' + $scope.selectedGame.id)
                            .success(function (response) {
                                if (! response) {
                                    $http.get('/api/v1/games/discount/' + $scope.selectedGame.id)
                                        .success(function (response) {
                                            $scope.selectedGame.discount_price = response;
                                            $scope.$apply;
                                        });
                                }
                            });

                        $('.modal').animate({
                            'opacity': 1
                        }, 250);
                    });
            } else {
                $('.modal').css('opacity', 0);
                $scope.selectedGame = {};
                $scope.quantity = 1;
            }
        };

        $scope.addToCart = function () {
            console.log($scope.quantity);

            if ($scope.quantity === undefined) {
                $scope.quantityError = true;
                return;
            }

            $scope.quantityError = false;

            $http.post('/api/v1/shopping-cart/add-game', {
                id: $scope.selectedGame.id,
                quantity: $scope.quantity
            }).success(function (response) {
                $scope.modalButtonText = 'Uw keuze is toegevoegd aan uw winkelwagen';
                $timeout(function () {
                    $scope.modalToggle();
                    $scope.modalButtonText = 'Voeg toe aan winkelwagen';
                },1000);
            });
        };

        $scope.addToFavoritesList = function (id) {
            $http.post('/api/v1/favorite-list/store', {
                id: id
            }).success(function (response) {
                if (response.message) {
                    $scope.favoriteListButtonText = response.message;
                }

                $scope.favoriteListButtonText = 'Toegevoegd';
            });
        };

        $scope.addToFavoritesListViaHome = function (id) {
            $scope.addToFavoritesList(id);

            $('#game-added-' + id).text('Toegevoegd').removeClass('is-hidden');
        };
    });

    // Game Review Controller
    gameshop.controller('GameReviewController', function ($scope, $http) {
        $scope.formOpen = false;
        $scope.review = {
            title: '',
            body: '',
            rating: 0,
            gameId: 0
        };
        $scope.reviews = [];

        $scope.init = function ($gameId) {
            $scope.review.gameId = $gameId;

            $http.get('/api/v1/reviews/' + $scope.review.gameId)
                .success(function (response) {
                    $scope.reviews = response;
                });
        };

        $scope.setRating = function (value) {
            $scope.review.rating = value;
        };

        $scope.toggleForm = function () {
            $scope.formOpen = !$scope.formOpen;
        };

        $scope.emptyReview = function () {
            $scope.review.title = '';
            $scope.review.body = '';
            $scope.review.rating = 0;
        };

        $scope.submitReview = function () {
            $http.post('/api/v1/reviews', $scope.review)
                .success(function (response) {
                    $scope.reviews.push(response);
                    $scope.toggleForm();
                    $scope.emptyReview();
                });
        };
    });

    // Game Screenshot Controller
    gameshop.controller('GameScreenshotController', function ($scope, $http) {
        $scope.gameId = 0;
        $scope.screenshots = [];

        $scope.init = function ($gameId) {
            $scope.gameId = $gameId;

            $http.get('/api/v1/screenshots/' + $scope.gameId)
                .success(function (response) {
                    $scope.screenshots = response;
                });
        };

        $scope.$on('ngRepeatFinished', function () {
            $("#lightgallery").lightGallery();
        });
    });

}(jQuery, angular, angularMasonry, lightGallery));