var express = require('express');
var app = express();

var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');

var url = 'mongodb://127.0.0.1:27017/leverancierGameshop';

var insert = function(db, callback, order) {

    var collection = db.collection('orders');
    collection.insert(order);
}

app.get('/store/order', function (req, res) {
    var order = JSON.parse(req.query.q);

    MongoClient.connect(url, function(err, db) {
        console.log("Connected correctly to server");

        insert(db, function() {
            db.close();
        },  order);
    });

    res.setHeader('Content-Type', 'application/json');
    res.send(req.query.q);
});

app.listen(3000);